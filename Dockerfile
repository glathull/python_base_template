FROM python:3.10.1-slim-buster AS base

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

FROM base AS python-deps

# Install poetry
RUN pip install poetry==1.1.11

# Install python dependencies in /.venv
COPY pyproject.toml .
COPY poetry.lock .
RUN POETRY_VIRTUALENVS_IN_PROJECT=true poetry install --no-dev

FROM base AS runtime

# Copy virtual env from python-deps stage
COPY --from=python-deps /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

# Create and switch to a new user
RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

# Install application into container
COPY . .

# Run the executable
ENTRYPOINT ["python", "-m", "main"]
