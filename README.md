[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

# Python Base Template
Clone (or fork and clone) this repo into a target directory with the name of your new project:

`git clone git@bitbucket.org:glathull/python_base_template.git <new_name>`

## Homebrew for mac:

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Pyenv:

**Mac:**
```
brew update
brew install pyenv
source ~/.zshrc
```

*M1 Macs sometimes have a problem with the Poetry install and need to add

`export PATH="PYENV_ROOT/shims:$PATH`

to your .zshrc file if pyenv isn't changing virtualenvs the way you expect.

**Linux:**

curl https://pyenv.run | bash

**Windows:**

Use WSL2 and follow Linux command


**pyenv setup for project:**
```
pyenv install 3.10.1
pyenv virtualenv 3.10.1 <name>
pyenv local <name>
pip install --upgrade pip
```

## Poetry:

Install:
```
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```


Set poetry environment:

```
poetry env use python
poetry shell
```

`exit` to leave poetry virtualenv

when outside the poetry env use `poetry run <name>` to execute a package installed. Example `poetry run bpython` will take you into the poetry venv and run bpython on top of that.

to add packages as project dependencies:

`poetry add <package-name>`

to add packages as dev dependencies:

`poetry add --dev <package_name>`

run tests:

`poetry run pytest`

for coverage assessment:

`poetry run pytest --cov=app/ tests/`


install git pre-commit hooks:
```
pre-commit install
```

All that's really left to do now is make the project your own.


edit pyproject.toml and update the following fields:


name:

version:

description:

authors:

After that's complete, you should be able to `poetry install` and have a fully functional Python app template with lots of supportive tooling (with sane defaults) and be on your way!


Some additional features:

1. dotenv to hold secrets and settings. Ignored locally, but a sample file provides the structure for others to build off of when you share the file. The `app.config.py` holds a settings object you can update with your own settings, and if any values are missing, it will let you know that you've forgotten something in the env file.

2. A production-class Dockerfile. Pulls from python-slim, with appropriate build stages and a reliable dependency build process, thanks to Poetry.

3. A sane structure that encourages you not to twist your head into knots with import errors. `main.py` is the entry point for the app (even if it's just a script). All of your dev and testing from your virtualenv can happen from the root directory of this repo, and `python main.py` will work. The important thing to remember here is to always use absolute imports inside your app code. Do not use relative imports, and everything will be fine. The Docker file is set up to handle this structure seamlessly.

4. Shell scripts! Because I can never remember the exact invocations for the Docker commands. You can (and probably should) run `./precheck.sh` to run all the pre-commit hooks before you stage or commit. The pre-commit hooks will modify files to normalize line endings and check to see if you are accidentally uploading some huge data file that you probably didn't mean to.

5. Some bare bones testing to at least make sure the app runs and everything imports correctly and a couple of samples for importing and running classes and methods. Just a jumping off point so you don't have to start from scratch.

6. Really great logging from https://github.com/Delgan/loguru#readme. Default setup here is to log to the terminal **AND** to a file so that you can trace a history. It's set up here to rotate every 50 MB and it stores logs entries as JSON for exporting and searching as well as raw for you to read. Never `print` again!
