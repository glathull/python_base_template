from app.logging import logger
from app.utils import divide_two_numbers, multiply_two_numbers


class TestClass:
    def __init__(self) -> None:
        self.a = 42
        self.b = 42
        self.c = 0

    def test_method(self) -> None:
        res = multiply_two_numbers(self.a, self.b)
        print("result: ", res)

    @logger.catch
    def test_log_catch(self) -> None:
        res = divide_two_numbers(self.a, self.c)
        print("result: ", res)
