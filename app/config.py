from pydantic import BaseSettings


class Settings(BaseSettings):
    SENTINAL: str
    API_USER_NAME: str
    API_PASSWORD: str

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
