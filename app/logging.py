import sys

from loguru import logger as _logger

config = {
    "handlers": [
        {
            "sink": sys.stdout,
            "format": "{time:!UTC} | {level} | {module} | {name} | {message}",
        },
        {"sink": "json_logs.log", "serialize": True, "rotation": "50 MB"},
        {"sink": "raw_logs.log", "serialize": False, "rotation": "50 MB"},
    ]
}

logger = _logger
logger.configure(**config)
