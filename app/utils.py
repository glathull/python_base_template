from numbers import Real

from app.logging import logger


def multiply_two_numbers(a: int | Real, b: int | Real) -> int | Real:
    logger.info(f"Multiplying {a} and {b}")
    return a * b


def divide_two_numbers(a: int | Real, b: int | Real) -> float | Real:
    logger.info(f"Dividing {a} by {b}")
    return a / b
