from app.classes import TestClass


def run_app() -> None:
    tc = TestClass()
    tc.test_method()


if __name__ == "__main__":
    run_app()
