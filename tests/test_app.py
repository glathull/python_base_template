from app import __version__
from app.utils import multiply_two_numbers
from main import run_app


def test_version():
    assert __version__ == "0.1.0"


def test_multiply_two_numbers():
    result = multiply_two_numbers(2, 3)
    assert result == 6


def test_env_vars():
    from app.config import settings

    assert settings.SENTINAL == "present"


def test_main_func():
    main_res = run_app()
    assert main_res is None
